import random
from scipy.interpolate import lagrange
import numpy as np 

xran = np.random.rand(17,1)
x = np.linspace(0,1,17)
for i in range(17):
    x[i] =xran[i]
x_sorted = np.sort(x,axis = 0)
print(x_sorted)
y = np.sin(x_sorted)
f = lagrange(x_sorted, y)


x_new = np.random.rand(10,1)
y_new = np.sin(x_new)
from numpy.polynomial.polynomial import Polynomial
print(f)
#plt.plot(x_test,model(x_test))
#print( Polynomial(poly.coef[::-1])(x_new))

from sklearn.metrics import mean_squared_error
#mean_squared_error(y, Polynomial(model.coef[::-1])(x_sorted))
print('Train error', mean_squared_error(y, f(x_sorted)))

print('Test error', mean_squared_error(y_new, f(x_new)))
ax.scatter(y_new, f(x_new))
std = [0.5, 1, 5, 10,20]

err_train=[]
err_test = []

for i in range(5): 
    e = np.random.normal(0,std[i],17)
    for i in range(17):
        x[i] += e[i]
    x_sorted = np.sort(x,axis = 0)
    y = np.sin(x_sorted)
    fn = lagrange(x_sorted, y)
    print('Train error\n', mean_squared_error(y, fn(x_sorted)))

    print('Test error\n', mean_squared_error(y_new, fn(x_new)))
    err_train.append(mean_squared_error(y, fn(x_sorted)))
    err_test.append( mean_squared_error(y_new, fn(x_new)))
    
plt.plot(std,err_train,'r', label= 'Training error')
plt.plot(std,err_test,'b', label = 'testing error')
plt.yscale('log')
plt.xlabel('Standard deviation of added noise')
plt.ylabel('err_n')
plt.legend()